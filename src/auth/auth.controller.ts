/**
 * 회원가입과 로그인 API 라우터가 구현되어있는 컨트롤러입니다.
 * // TODO: 5. [hands-on] Code Suggestions 의 도움을 받아 아웃라인을 작성해보세요.
 * // TODO: 6. [hands-on] Code Suggestions 의 도움을 받아 작성된 SignUpDto, LoginDto 를 사용해보세요.
 */
import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignUpDto } from './dto/signUp.dto';
import { LoginDto } from './dto/login.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  // 회원가입을 위한 API
  
  // 로그인을 위한 API
}
